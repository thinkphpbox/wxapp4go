package wxapp

import (
	"encoding/json"
	"errors"
	"fmt"
)

type EncryptedCheckRet struct {
	WxApiRet
	//是否是合法的数据
	Vaild      bool 	`json:"vaild"`
	//加密数据生成的时间戳
	CreateTime int64 	`json:"create_time"`
}

//检查加密信息是否由微信生成（当前只支持手机号加密数据），只能检测最近3天生成的加密数据
func EncryptedCheck(access_token string, encrypted_msg_hash string) (EncryptedCheckRet, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/business/checkencryptedmsg"
	wx_addr += "?access_token=" + access_token

	var ret EncryptedCheckRet
	paramstr := fmt.Sprintf("{\"encrypted_msg_hash\":\"%s\"}", encrypted_msg_hash)
	res, err := WxApiPost(wx_addr, []byte(paramstr))
	if err != nil {
		return ret, err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return ret, err
	}

	return ret, nil
}

type GetPaidInfoReq struct {
	OpenId 			string 		`json:"openid"`
	TransactionId 	string 		`json:"transaction_id"`
	MchId 			string 		`json:"mch_id"`
	OuTradeNo		string 		`json:"out_trade_no"`
}

type GetPaidInfoRet struct {
	WxApiRet
	UnionId 		string		`json:"unionid"`
}

//用户支付完成后，获取该用户的 UnionId，无需用户授权
func GetPaidInfo(access_token string, param GetPaidInfoReq) (GetPaidInfoRet, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/getpaidunionid"
	wx_addr += "?access_token=" +access_token

	var ret GetPaidInfoRet
	res, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return ret, err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return ret, err
	}

	return ret, nil
}

type GetPluginPidRet struct {
	WxApiRet
	//插件用户的唯一标识
	OpenpId      string 	`json:"openpid"`
}

//通过 wx.pluginLogin 接口获得插件用户标志凭证 code 后传到开发者服务器，开发者服务器调用此接口换取插件用户的唯一标识 openpid。
func GetPluginPid(access_token string, code string) (string, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/getpluginopenpid"
	wx_addr += "?access_token=" + access_token

	var ret GetPluginPidRet
	paramstr := fmt.Sprintf("{\"code\":\"%s\"}", code)
	res, err := WxApiPost(wx_addr, []byte(paramstr))
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return "", err
	}

	if ret.ErrCode != 0 {
		return "", errors.New(ret.ErrMsg)
	}

	return ret.OpenpId, nil
}

type WxWaterMark struct {
	//敏感数据归属 appId，开发者可校验此参数与自身 appId 是否一致
	AppId 			string 			`json:"appid"`
	//敏感数据获取的时间戳, 开发者可以用于数据时效性校验
	TimeStamp 		int				`json:"timestamp"`
}

type PhoneInfo struct {
	//用户绑定的手机号（国外手机号会有区号）
	PhoneNumber 	string 			`json:"phoneNumber"`
	//没有区号的手机号
	PurePhoneNumber string			`json:"purePhoneNumber"`
	//区号
	CountryCode 	string			`json:"countryCode"`
	WaterMark		WxWaterMark		`json:"watermark"`
}

type GetPhoneNumberRet struct {
	WxApiRet
	PhoneInfo		PhoneInfo 	`json:"phone_info"`
}

//code换取用户手机号。
//每个code只能使用一次，code的有效期为5min
func GetPhoneNumber(access_token string, code string) (GetPhoneNumberRet, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/business/getuserphonenumber"
	wx_addr += "?access_token=" + access_token

	var ret GetPhoneNumberRet
	paramstr :=fmt.Sprintf("{\"code\":\"%s\"}", code)
	res, err := WxApiPost(wx_addr, []byte(paramstr))
	if err != nil {
		return ret, err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return ret, err
	}

	return ret, nil
}

//解密用户手机号数据
func DecodePhoneNumber(rawData, key, iv string) (PhoneInfo, error) {
	var ret PhoneInfo
	retstr, err := WxDncrypt(rawData, key, iv)
	if err != nil {
		return ret, nil
	}

	err = json.Unmarshal([]byte(retstr), &ret)
	if err != nil {
		return ret, err
	}

	return ret, nil
}

