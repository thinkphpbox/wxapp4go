package wxapp

import (
	"encoding/json"
)

type ShortLinkReq struct {
	//通过 Short Link 进入的小程序页面路径，必须是已经发布的小程序存在的页面，可携带 query，最大1024个字符
	PageUrl        	string 		`json:"page_url"`
	//页面标题，不能包含违法信息，超过20字符会用... 截断代替
	PageTitle		string 		`json:"page_title"`
	//生成的 Short Link 类型，短期有效：false，永久有效：true
	IsPermanent		bool     	`json:"is_permanent"`
}

type ShortLinkRet struct {
	WxApiRet
	Link 			string 		`json:"link"`
}

//获取小程序 Short Link，
//适用于微信内拉起小程序的业务场景。
//通过该接口，可以选择生成到期失效和永久有效的小程序短链
func CreateShortLink(access_token string, param ShortLinkReq) (string, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/genwxashortlink"
	wx_addr += "?access_token=" + access_token

	var ret ShortLinkRet
	res, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return "", err
	}

	return ret.Link, nil
}

func CreateShortLinkTemp(access_token string, page_url string, page_title string) (string, error) {
	return CreateShortLink(access_token, ShortLinkReq{
		PageUrl:page_url,
		PageTitle:page_title,
		IsPermanent: false,
	})
}

func CreateShortLinkPermanent(access_token string, page_url string, page_title string) (string, error) {
	return CreateShortLink(access_token, ShortLinkReq{
		PageUrl:page_url,
		PageTitle:page_title,
		IsPermanent: true,
	})
}