package wxapp

import (
	"encoding/json"
	"errors"
	"fmt"
)

type WxApiTokenRet struct {
	WxApiRet
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
}

//获取小程序全局唯一后台接口调用凭据（access_token）。
//调用绝大多数后台接口时都需使用 access_token，开发者需要进行妥善保存。
func getAccessToken(appid string, secret string) (WxApiTokenRet, error) {
	wx_api_url := "https://api.weixin.qq.com/cgi-bin/token"
	wx_api_url = fmt.Sprintf("%s?grant_type=client_credential&appid=%s&secret=%s",
		wx_api_url, appid, secret)

	var ent WxApiTokenRet
	res, err := WxApiGet(wx_api_url)
	if err != nil {
		return ent, err
	}

	if err := json.Unmarshal(res, &ent); err != nil {
		return ent, err
	}

	if ent.ErrCode != 0 {
		return ent, errors.New(ent.ErrMsg)
	}
	return ent, nil
}
