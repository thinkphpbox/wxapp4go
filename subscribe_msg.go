package wxapp

import "encoding/json"

//媒体文件上传返回结果
type UploadTempMediaRet struct {
	WxApiRet
	FileType 		string 		`json:"type"`
	MediaId 		string 		`json:"media_id"`
	CreatedTime		int64 		`json:"created_at"`
}

//把媒体文件上传到微信服务器,目前仅支持图片
func UploadTempMedia(access_token string, file_path string) (UploadTempMediaRet, error) {
	wx_addr := "https://api.weixin.qq.com/cgi-bin/media/upload"
	wx_addr += "?access_token=" + access_token
	wx_addr += "&type=image"

	var ret UploadTempMediaRet
	result, err := PostFileByFormData(wx_addr, nil, file_path, "media")
	if err != nil {
		return ret, err
	}

	err = json.Unmarshal([]byte(result), &ret)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

//获取客服消息内的临时素材。即下载临时的多媒体文件。目前小程序仅支持下载图片文件。
//如果调用成功，会直接返回图片二进制内容，如果请求失败，会返回 JSON 格式的数据。
func GetTempMedia(access_token string, media_id string) ([]byte, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/getwxacodeunlimit"
	wx_addr += "?access_token=" + access_token
	wx_addr += "&media_id=" + media_id
	res, err := WxApiGet(wx_addr)
	if err != nil {
		return nil, err
	}
	return res, nil
}

type TemplData map[string]TemplItem
type SubscribeMsgReq struct {
	//接收者（用户）的 openid
	Touser  	string			`json:"touser"`
	//所需下发的订阅模板id
	TemplID		string			`json:"template_id"`
	//点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
	Page 		string			`json:"page"`
	//模板内容，格式形如 { "key1": { "value": any }, "key2": { "value": any } }
	TemplData 	TemplData 		`json:"data"`
	//跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
	AppEnv		string  		`json:"miniprogram_state"`
	//支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN
	Lang		string  		`json:"lang"`
}

type TemplItem struct {
	Value 		string 			`json:"value"`
}

//发送微信订阅消息
func SendSubscribeMsg(access_token string, param SubscribeMsgReq) error {
	wx_addr := "https://api.weixin.qq.com/cgi-bin/message/subscribe/send"
	wx_addr += "?access_token=" + access_token
	_, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return  err
	}
	return nil
}
